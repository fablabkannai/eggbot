|Qty|Value|Device|Package|Parts|Description|BUILT_BY|MANUFACTURER_PART_NUMBER|MF|MPN|OC_FARNELL|OC_NEWARK|VENDOR|
|--|--|--|--|--|--|--|--|--|--|--|--|--|
|1| |PINHD-1X3|1X03|JP3|PIN HEADER| | | | | | | |
|2| |PINHD-1X5|1X05|JP1, JP2|PIN HEADER| | | | | | | |
|1| |SW_SWITCH_TACTILE_6MM6MM_SWITCH|6MM_SWITCH|S1|OMRON SWITCH| | | | | | | |
|5|0 ohm|R1206FAB|R1206FAB|R3, R5, R6, R7, R8|Resistor (US Symbol)| | | | | | | |
|1|0.1uF|CAP_UNPOLARIZEDFAB|C1206FAB|C4| | | | | | | | |
|1|10k|R1206FAB|R1206FAB|R2|Resistor (US Symbol)| | | | | | | |
|3|10uF|CAP_UNPOLARIZEDFAB|C1206FAB|C3, C5, C6| | | | | | | | |
|1|16MHz|CSM-7X-DU|CSM-7X-DU|XTAL|SMD CRYSTAL| | | | |unknown|unknown| |
|1|2.7K|R1206FAB|R1206FAB|R1|Resistor (US Symbol)| | | | | | | |
|2|22pF|CAP_UNPOLARIZEDFAB|C1206FAB|C1, C2| | | | | | | | |
|1|ATMEGA328P|ATMEGA328P-AU32A-L|32A-L|PD3| |EMA_UL_Team|ATMEGA328P-AU| | | | |Atmel|
|1|CONN_06_FTDI-SMD-HEADER|CONN_06_FTDI-SMD-HEADER|1X06SMD|FTDI| | | | | | | | |
|1|ISP|CONN_03X2_AVRISPSMD|2X03SMD|U$3| | | | | | | | |
|1|JACK-2.1MM|JACK-2.1MM|PJ-002AH-SMT|J2|SMD DC power jack PJ-002AH-SMT As found in the fablab inventory.| | | | | | | |
|1|LED|LEDFAB1206|LED1206FAB|LED|LED| | | | | | | |
|2|MICRO-USB|CONN_MICRO-USB_1/64|DX4R005HJ5_64|MOTOR, VCC|SMD micro USB connector as found in the fablab inventory.| | | | | | | |
|2|ULN2003AN|ULN2003AN|DIL16|IC1, IC2|DRIVER ARRAY| | | | | | | |

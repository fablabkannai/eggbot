# How to make EggBot

<!--
![](img/eggbot.jpg)
-->

## 

## BOM
- Stepper Moter + driver
    - [HiLetgo 5個セット 5V ステッピングモータ+ ULN2003ドライバーボード セット](https://www.amazon.co.jp/HiLetgo%C2%AE-5%E5%80%8B%E3%82%BB%E3%83%83%E3%83%88-%E3%82%B9%E3%83%86%E3%83%83%E3%83%94%E3%83%B3%E3%82%B0%E3%83%A2%E3%83%BC%E3%82%BF-ULN2003%E3%83%89%E3%83%A9%E3%82%A4%E3%83%90%E3%83%BC%E3%83%9C%E3%83%BC%E3%83%89-%E4%B8%A6%E8%A1%8C%E8%BC%B8%E5%85%A5%E5%93%81/dp/B010RYH74U/ref=pd_aw_sbs_328_2/356-8054961-4400152?_encoding=UTF8&pd_rd_i=B010RYH74U&pd_rd_r=3473992a-4622-4954-8e2f-84f45f46c5e7&pd_rd_w=9XzM8&pd_rd_wg=pMPoM&pf_rd_p=1893a417-ba87-4709-ab4f-0dece788c310&pf_rd_r=D2ZATD9PTDAK52XSSVZE&psc=1&refRID=D2ZATD9PTDAK52XSSVZE)   
- micro servo
    - [SG90](https://www.amazon.co.jp/TOWER-PRO-SG90-D-5-%E3%83%87%E3%82%B8%E3%82%BF%E3%83%AB%E3%83%BB%E3%83%9E%E3%82%A4%E3%82%AF%E3%83%AD%E3%82%B5%E3%83%BC%E3%83%9C-SG90/dp/B00VUJYNWG/ref=sr_1_3?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&keywords=SG90&qid=1575040034&sr=8-3)
- parts list

|Qty|Value|Device|Package|Parts|Description|BUILT_BY|MANUFACTURER_PART_NUMBER|MF|MPN|OC_FARNELL|OC_NEWARK|VENDOR|
|--|--|--|--|--|--|--|--|--|--|--|--|--|
|1| |PINHD-1X3|1X03|JP3|PIN HEADER| | | | | | | |
|2| |PINHD-1X5|1X05|JP1, JP2|PIN HEADER| | | | | | | |
|1| |SW_SWITCH_TACTILE_6MM6MM_SWITCH|6MM_SWITCH|S1|OMRON SWITCH| | | | | | | |
|5|0 ohm|R1206FAB|R1206FAB|R3, R5, R6, R7, R8|Resistor (US Symbol)| | | | | | | |
|1|0.1uF|CAP_UNPOLARIZEDFAB|C1206FAB|C4| | | | | | | | |
|1|10k|R1206FAB|R1206FAB|R2|Resistor (US Symbol)| | | | | | | |
|3|10uF|CAP_UNPOLARIZEDFAB|C1206FAB|C3, C5, C6| | | | | | | | |
|1|16MHz|CSM-7X-DU|CSM-7X-DU|XTAL|SMD CRYSTAL| | | | |unknown|unknown| |
|1|2.7K|R1206FAB|R1206FAB|R1|Resistor (US Symbol)| | | | | | | |
|2|22pF|CAP_UNPOLARIZEDFAB|C1206FAB|C1, C2| | | | | | | | |
|1|ATMEGA328P|ATMEGA328P-AU32A-L|32A-L|PD3| |EMA_UL_Team|ATMEGA328P-AU| | | | |Atmel|
|1|CONN_06_FTDI-SMD-HEADER|CONN_06_FTDI-SMD-HEADER|1X06SMD|FTDI| | | | | | | | |
|1|ISP|CONN_03X2_AVRISPSMD|2X03SMD|U$3| | | | | | | | |
|1|JACK-2.1MM|JACK-2.1MM|PJ-002AH-SMT|J2|SMD DC power jack PJ-002AH-SMT As found in the fablab inventory.| | | | | | | |
|1|LED|LEDFAB1206|LED1206FAB|LED|LED| | | | | | | |
|2|MICRO-USB|CONN_MICRO-USB_1/64|DX4R005HJ5_64|MOTOR, VCC|SMD micro USB connector as found in the fablab inventory.| | | | | | | |
|2|ULN2003AN|ULN2003AN|DIL16|IC1, IC2|DRIVER ARRAY| | | | | | | |

[downloadCSV](docs/bom.csv)

- body

|name|size|Qty|
|----|----|---|
|MDF2.5|300x450|1|
|Bolt|M3x10|5|
|Bolt|M6x150|2|
|Nut|M3|5|
|Nut|M6|1|


## hardware
- [eggbot_laser_mdf2_5.dxf](eggbot_laser_mdf2_5.dxf )

## How to make
### make PCB
#### Eagle file
![SCH](img/sch.png)
![BRd](img/brd.png)  

- [SCH](code/eggbot.sch)  
- [BRD](code/eggbot.brd)  

#### Mill
![top](img/top.png)
![holes](img/holes.png)
![outline](img/outline.png)  
[download](code/eggbot_mill.zip)
![boardpic](img/board.jpg)

#### GRBL
[download](code/eggbot_grbl.zip)
- load job file > ...> SeedFusion_2_layer.cam
- SilkscreenTop > add 200bmp 
![](img/seedcam.png)
![](img/200bmp.png)

### make body
[download](code/eggbot_laser_mdf2_5.dxf)
![](img/eggbot.jpg)
#### Assemble
![](assemble.jpg)

#### connection
![](connection.jpg)


## software
### files
- [Eggguino](code/Eggduino)
- [Inkscape_extension_only](code/Inkscape_extension_only)
- [inkscape/modified_files](code/inkscape/modified_files)

### install
#### bootloader
- Arduino IDE
    - Arduino IDE > Preference >board manager
        -  https://mcudude.github.io/MiniCore/package_MCUdude_MiniCore_index.json
    -  select
        - Arduino：1.8.10 (Windows 10)
        - ボード："ATmega328"
        - 16 MHz external
- ISP
    - Arduino Uno  as ISP
    - burn bootloader   

#### firmware
- [Eggguino](code/Eggduino)
- Arduino IDE
    - Arduino UNO  as ISP
    - 書き込み装置を使って書く

## draw
### software
- Inkscape
    - [Inkscape_extension_only](code/Inkscape_extension_only)
    - [inkscape/modified_files](code/inkscape/modified_files)
- FTDI USB-serial
- port
    - windows
        - device manager
        - COM?
    - mac
        - ```$ ls /dev | grep usb```
        - /dev/tty.usb*
 - path
    - open > Inkscape template
    - draw line
 - select externtion
 
